angular.module('app')
    .factory('HistorialResource', function ($resource, $window) {
        const usuario = JSON.parse($window.sessionStorage.getItem('usuario'));
        return $resource('http://localhost:3003/historial/:id', { id: '@id' },
            {
                query: {
                    method: 'GET',
                    isArray: true,
                    headers: { 'Authorization': usuario.token }
                },
                get: {
                    method: 'GET',
                    headers: { 'Authorization': usuario.token }
                }
            });
    })
    .factory('ComprasResource', function ($resource, $window) {
        const usuario = JSON.parse($window.sessionStorage.getItem('usuario'));
        return $resource('http://localhost:3003/historial/yo', {},
            {
                query: {
                    method: 'GET',
                    isArray: true,
                    headers: { 'Authorization': usuario.token }
                }
            });
    });